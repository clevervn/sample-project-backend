const AuthenticationError = require('./AuthenticationError');
const ResourceNotFoundError = require('./ResourceNotFoundError');

module.exports = {
  AuthenticationError,
  ResourceNotFoundError,
};
